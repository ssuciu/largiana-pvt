

$("#price").on('change',function () {
    var value = $("#price").val();
    sessionStorage.setItem('largiana.price', value);
});

$('#weight').on('change', function () {
    var weight = $(this).val();
    sessionStorage.setItem('largiana.weight', weight);
});

var price = sessionStorage.getItem('largiana.price');
if (price) {
    $("#price").val(price);
}
var weight = sessionStorage.getItem('largiana.weight');
if (weight) {
    $('#weight').val(weight);
}
var notificationOptionsInfo = {
    animate: {
        enter: 'animated fadeInDown',
        exit: 'animated fadeOutUp'
    },
    type: 'info',
    placement : {
        from : 'bottom',
        align: 'right'
    }
};

var notificationOptionsDanger = {
    animate: {
        enter: 'animated fadeInDown',
        exit: 'animated fadeOutUp'
    },
    type: 'danger',
    placement : {
        from : 'bottom',
        align: 'right'
    }
};

function gedDiff() {
    var optim = 100;
    var $volumes = $(".volume");
    var sum = 0;
    for (var i = 0; i< $volumes.length; i++) {
        sum += parseFloat($volumes[i].value);
    }
    sum = sum.toFixed(3) * 100;
    return sum - optim;
}

function sumVolume() {
    var optim = 100;
    var displayArea = $("#up-notifications");
    var $volumes = $(".volume");
    var sum = 0;
    for (var i = 0; i< $volumes.length; i++) {
        sum += parseFloat($volumes[i].value);
    }
    sum = sum.toFixed(3) * 100;
    var diff = sum - optim;
    if(diff > 0) {
        var message = "+" + diff.toFixed(3) + "% la volum.";
        displayArea.html(message);
        $.notify({message : message}, notificationOptionsInfo);
    } else if (diff < 0) {
        var message = diff.toFixed(3) + "% la volum.";
        displayArea.html(message);
        $.notify({message: message}, notificationOptionsInfo);
    } else {
        var input = $("#form").serialize();
        $.ajax({
            method: 'POST',
            url: '/process',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data : input,
            success: function (data) {
                $.notify({
                    message : data
                }, notificationOptionsInfo);

                $("#up-notifications").html(data);
            },
            error: function (data) {
                $.notify({
                    message : data.responseText
                }, notificationOptionsDanger);
                $("#up-notifications").html(data.responseText);
            }
        });
    }
}
$(".volume").on('change', function () {
    sumVolume();
});

$(".price").on('change', function () {
    var optim = 100;
    var displayArea = $("#up-notifications");
    var $volumes = $(".volume");
    var sum = 0;
    for (var i = 0; i< $volumes.length; i++) {
        sum += parseFloat($volumes[i].value);
    }
    sum = sum.toFixed(3) * 100;
    var diff = sum - optim;
    if(diff === 0) {
        var input = $("#form").serialize();
        $.ajax({
            method: 'POST',
            url: '/process',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data : input,
            success: function (data) {
                $.notify({
                    message : data
                }, notificationOptionsInfo);

                displayArea.html(data);
            },
            error: function (data) {
                $.notify({
                    message : data.responseText
                }, notificationOptionsDanger);
                displayArea.html(data.responseText);
            }
        });
    }
});