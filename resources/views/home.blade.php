@extends('layouts.app')

@section('extra-header-scripts')
    <link rel="stylesheet" type="text/css" href="{{asset('css/datepicker.min.css')}}">
    <style>
        .error {
            color: #b81919;
        }
    </style>
@endsection


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading" style="display: flex; justify-content: space-between">
                        <div>Panou principal</div>
                        <div><b id="up-notifications"></b></div>
                    </div>

                    <div class="panel-body">
                        <ul class="nav nav-tabs nav-justified">
                            <li id="li1" class="nav-item active">
                                <a class="nav-link active" data-toggle="tab" id="nav2" href="#tab1">Preturi</a>
                            </li>
                            <li id="li2" class="nav-item">
                                <a class="nav-link"data-toggle="tab" id="nav1" href="#tab2">Date proces verbal</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab1" class="tab-pane fade in active">

                                <form id="form">
                                    <div class="row" style="margin-bottom: 20px">
                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                            <label for="basic-url">&nbsp;</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="price-lbl">Pret</span>
                                                <input type="number" name="price" class="form-control" id="price" aria-describedby="price-lbl">
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-6">
                                            <label for="basic-url">&nbsp;</label>
                                            <div class="input-group">
                                                <span class="input-group-addon" id="weight-lbl">Cantitate</span>
                                                <input type="number" name="weight" class="form-control" id="weight" aria-describedby="weight-lbl">
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>Nume piesa</th>
                                            <th>Procent volum</th>
                                            <th>Pret cu TVA</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($products as $product)
                                            <tr>
                                                <td>{{$product->name}}</td>
                                                <td data-value="{{$product->volume}}">
                                                    <input class="form-control volume" name="product[{{$product->id}}][volume]" min="0" type="number" step="0.005" value="{{$product->volume}}">
                                                </td>
                                                <td>
                                                    <input class="form-control price" name="product[{{$product->id}}][price]" min="0" type="number" step="0.1" value="{{$product->price}}">
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                            <div id="tab2" class="tab-pane fade form-horizontal">
                                <br />
                                <form id="details-form">
                                    <div class="form-group">
                                        <label for="nrFactura"  class="col-sm-3 control-label">Nr. factura materie prima</label>
                                        <div class="col-sm-9">
                                            <input type="number" required min="0" name="nrFactura" class="form-control" id="nrFactura">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="nrProc" class="col-sm-3 control-label">Nr. proces verbal</label>
                                        <div class="col-sm-9">
                                            <input type="number" required min="0" name="nrProc" class="form-control" id="nrProc">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="lot" class="col-sm-3 control-label">Lot</label>
                                        <div class="col-sm-9">
                                            <input type="number" min="0" required name="lot" class="form-control" id="lot">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="date" class="col-sm-3 control-label">Data</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="date" required class="form-control" id="date">
                                        </div>
                                    </div>
                                </form>
                                <button id="generate" style="float: right" class="btn btn-primary">Genereaza proces verbal</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra-footer-scripts')
    <script src="{{asset("js/bootstrap.min.js")}}"></script>
    <script src="{{asset("js/validate.min.js")}}"></script>
    <script src="{{asset("js/validate.locale.ro.js")}}"></script>
    <script src="{{asset("js/datepicker.min.js")}}"></script>
    <script src="{{asset('js/datepicker.locale.ro.min.js')}}" charset="UTF-8"></script>
    <script src="{{asset("js/home.js")}}"></script>
    <script>
        sumVolume();
        $("#date").datepicker({
            'language' : 'ro',
            'autoclose': true
        });
        $("#generate").on('click', function () {
            var $form = $("#details-form");

            function moveTab() {
                $("#tab2").removeClass('in');
                $("#tab2").removeClass('active');
                $("#nav2").removeClass('active');
                $("#li2").removeClass('active');
                $("#tab1").addClass('in');
                $("#tab1").addClass('active');
                $("#nav1").addClass('active');
                $("#li1").addClass('active');
            }
            if(!$form.valid()) {
            } else {
                if(gedDiff() !== 0) {
                    $.notify({
                        message : "Corectati diferenta de volum inainte de generarea procesului verbal."
                    }, notificationOptionsDanger)
                } else {
                    var price = $("#price").val();
                    var weight = $("#weight").val();
                    if(!price || !weight) {
                        $.notify({
                            message : "Completati campul de pret si cantitate."
                        }, notificationOptionsDanger);
                        moveTab();
                        return;
                    }
                    var data = $("#details-form").serialize();
                    data += 'price=' + price;
                    data += '&weight=' + weight;
                    var url ="/page?" + data;
                    var win = window.open(url, '_blank');
                    win.focus();
                }
            }
        })
    </script>
@endsection
