<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>A4</title>

    <link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
    <!-- Normalize or reset CSS with your favorite library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

    <!-- Load paper.css for happy printing -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">

    <!-- Set page size here: A5, A4 or A3 -->
    <!-- Set also "landscape" if you need -->
    <style>
        @page { size: A4 }
    </style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A4">

<!-- Each sheet element should have the class "sheet" -->
<!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
<section style="font-family: 'Noto Sans', sans-serif;" class="sheet padding-20mm">

    <!-- Write HTML just like a web page -->
    <article style="display: flex;justify-content: space-between"><div>Unitatea: SC.Largiana Carn S.R.L</div><div>Se aproba</div></article>
    <br/>
    <article style="display: flex;justify-content: space-between"><div>Gestiunea: Magazin Bradet</div><div>Administrator</div><div>Contabil</div></article>

</section>

</body>

</html>