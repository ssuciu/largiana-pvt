<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            "Pulpa fara os/0.15/17.5",
            "Muchiulet/0.01/25.6",
            "Cotlet cu os/0.01/17",
            "Cotlet fara os/0.04/21.5",
            "Ceafa cu os/0.01/17",
            "Ceafa fara os/0.04/21.5",
            "Fleica/0.05/16.9",
            "Costita/0.04/16.5",
            "Ciolan/0.06/10",
            "Ciolan dezosat/0.02/16.9",
            "CPL/0.18/16",
            "Spata porc/0.04/17.5",
            "Slanina tablii/0.05/10",
            "Slanina lucru/0.14/6",
            "Oase garf/0.06/3.5",
            "Picioare/0.02/4.5",
            "Oase mici/0.1/1.5",
            "Sorici/0.13/1.5",
            "Deseu fara valoare/0.025/0"
        ];
        $hasData = !!count(\App\Product::all());

        if(!$hasData) {
            foreach ($data as $productString) {
                $arr = explode('/', $productString);
                $product = new \App\Product();
                $product->name = $arr[0];
                $product->volume = (float) $arr[1];
                $product->price = (float) $arr[2];
                $product->save();
            }
        }
    }
}
