<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Support\Facades\Input;

class ProcessController extends Controller
{

    public function process() {
        $input = Input::get();
        $price = $input['price'];
        $cantitate = $input['weight'];
        $products = $input['product'];
        $valoareVanzare = 0;


        if(!$price || !$cantitate || (float) $price <= 0 || (float) $cantitate <=0) {
            return response('Pretul sau cantitatea nu sunt setate corect', 400);
        }
        $cantitate = (float) $cantitate;
        $price = (float) $price;

        $valoareCumparare = (float) $cantitate * (float) $price;

        $preturi = [];
        foreach ($products as $id => $product) {
            $realPercent = (float) $product['volume'] * 100;
            $piecePrice = $realPercent * $cantitate * (float) $product['price'];
            $piecePrice = $piecePrice / 100;
            array_push($preturi, $piecePrice);
            $valoareVanzare += $piecePrice;
            Product::where('id', '=', $id)
                ->update([
                    'price' => $product['price'],
                    'volume' => $product['volume']
                    ]);
        }

        $adaosulComercial = $valoareVanzare / $valoareCumparare;
        $adaosulComercial = $adaosulComercial - 1;
        $adaosulComercial = $adaosulComercial * 100;
        $adaosulComercial = round($adaosulComercial, 2);
        return response('Adaos comercial este '. $adaosulComercial. '%', 200);
    }
}
